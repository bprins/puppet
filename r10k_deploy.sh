#!/usr/bin/env bash

echo "Invoke deployment with r10k API ..."
sleep 1
echo "202 requestid: 48ce0715-c437-4c5d-83a6-fe0023cdd2e2"
sleep 2
echo "Request status: IN_QUEUE"
sleep 5
for i in {1..5}
do
  echo "Request status: IN_PROGRESS"
  sleep 3
done
echo "Request status: SUCCESS"
